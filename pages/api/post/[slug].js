import nextConnect from 'next-connect';
import middleware from '../../../middleware/auth';
import { retreiveFile } from '../../../lib/file';
import models from '../../../db/models/index';

const handler = nextConnect()
  // Middleware
  .use(middleware)
  // Get method
  .get(async (req, res) => {
    //List of Post
    const {
      user: { id: userId },
      query: { page },
    } = req;
    if (page < 0) page = 0;
    let offset = (page - 1) * 3;
    models.posts
      .findAndCountAll({
        // where: {
        //   userId,
        // },
        order: [['createdAt', 'DESC']],
        include: [
          {
            model: models.documents,
          },
          {
            model: models.users,
            attributes: ['username'],
          },
        ],
        limit: [offset, 3],
      })
      .then(async posts => {
        let data = [];
        for (let i = 0; i < posts.rows.length; i++) {
          const { id, description, createdAt, document, user } = posts.rows[i];
          let file = '';
          if (document) {
            file = await retreiveFile(document.filePath + document.fileName);
          }
          data.push({
            id,
            description,
            url: file.url,
            username: user.username,
            createdAt,
          });
        }
        return res.status(200).json({ status: 'success', data });
      })
      .catch(err => {
        return res.status(400).json({ status: 'failed', err });
      });
  })
  // Post method
  .post(async (req, res) => {
    const { body: post } = req;
    const newPost = await models.posts.create(JSON.parse(post));
    return res.status(200).json({
      status: 'success',
      message: 'done',
      data: newPost,
    });
  })
  // Put method
  .put(async (req, res) => {
    res.end('method - put');
  })
  // Patch method
  .patch(async (req, res) => {
    throw new Error('Throws me around! Error can be caught and handled.');
  });

export default handler;
