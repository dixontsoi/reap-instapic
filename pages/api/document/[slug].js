import formidable from 'formidable';
import { uploadAWSFile } from '../../../lib/file';
import models from '../../../db/models/index';

export const config = {
  api: {
    bodyParser: false,
  },
};

export default async (req, res) => {
  const form = new formidable.IncomingForm();
  const chunks = [];
  form.keepExtensions = true;

  form.onPart = function(part) {
    if (!part.filename) {
      form.handlePart(part);
      return;
    }

    part.on('data', function(buffer) {
      chunks.push(buffer);
    });
  };
  form.parse(req, async (err, fields, files) => {
    let userId = JSON.parse(fields.user)['id'];
    let response = await uploadAWSFile({
      userId: userId,
      filename: fields.name,
      value: Buffer.concat(chunks),
    });
    const { success } = response;
    //Create Document
    if (success) {
      models.documents
        .create({
          filePath: userId + '/',
          fileName: fields.name,
        })
        .then(item => {
          res.statusCode = 200;
          return res.json({ status: 'success', data: item });
        })
        .catch(err => {
          return res.status(400).json(err);
        });
    }
  });
};
