import nextConnect from 'next-connect';
import models from '../../../db/models/index';

const handler = nextConnect()
  .get(async (req, res) => {
    const {
      query: { id, name },
    } = req;
    const { slug } = req.query;
    const userId = slug;
    const user = await models.users.findOne({
      where: {
        id: userId,
      },
      include: [
        {
          model: models.posts,
          as: 'posts',
        },
      ],
    });
    res.statusCode = 200;
    return res.json({ status: 'success', data: user });
  })
  .post(async (req, res) => {
    const { body } = req;
    const { username, email, password } = body;
    const user = await models.users.findOne({
      where: {
        email,
      },
    });
    if (user) {
      return res.status(400).json({
        status: 'error',
        error: 'The email already existed',
      });
    }
    const newUser = await models.users.create({
      username,
      email,
      password,
    });
    return res.status(200).json({
      status: 'success',
      message: 'done',
      data: newUser,
    });
  })
  .put(async (req, res) => {})
  .patch(async (req, res) => {});

export default handler;
