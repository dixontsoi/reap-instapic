import { useState, useEffect } from 'react';
import Link from 'next/link';
import PostList from '../components/post/List';
import { absoluteUrl, getAppCookies } from '../middleware/utils';
import Layout from '../components/layout/Layout';

export default function Home(props) {
  const { user, origin, baseApiUrl } = props;
  const [post, setPost] = useState([]);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);

  let fetchPost = async () => {
    const postApi = await fetch(`${baseApiUrl}/post/[slug]?page=${page}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
      },
    });
    let result = await postApi.json();
    setPost(preState => {
      return [...preState].concat(result.data);
    });
    setLoading(false);
  };

  let loadMorePage = () => {
    let nextPage = page;
    nextPage = nextPage + 1;
    setLoading(true);
    setPage(nextPage);
  };

  useEffect(() => {
    if (user) {
      fetchPost(page);
    }
  }, [page]);

  return (
    <Layout title="Home Page" url={origin} origin={origin} user={user}>
      <div className="container">
        <main>
          {user ? (
            <>
              <div className="grid p-3">
                <div className="col d-flex justify-content-end">
                  <Link href={{ pathname: '/post/create' }}>
                    <a type="button" className="btn btn-primary">
                      Create
                    </a>
                  </Link>
                </div>
              </div>
              {post.length > 0 ? (
                <div className="grid p-3">
                  <PostList
                    post={post}
                    loading={loading}
                    loadMorePage={loadMorePage}
                  />
                </div>
              ) : (
                <div className="grid p-3">
                  <p>No Post Yet</p>
                </div>
              )}
            </>
          ) : (
            <>
              <div className="position-absolute top-50 start-50 translate-middle">
                Have an Account? <br />
                <Link href={{ pathname: '/user/login' }}>
                  <a>Login </a>
                </Link>
                /
                <Link href={{ pathname: '/user/register' }}>
                  <a> Register</a>
                </Link>
              </div>
            </>
          )}
        </main>
      </div>
    </Layout>
  );
}
/* getServerSideProps */
export async function getServerSideProps(context) {
  const { req } = context;
  const { origin } = absoluteUrl(req);

  const baseApiUrl = `/api`;
  return {
    props: {
      origin,
      baseApiUrl,
    },
  };
}
