import Link from 'next/link';

function FormPost(props) {
  const {
    onSubmit,
    onChange,
    loading,
    stateFormData,
    stateFormMessage,
  } = props;
 
  const handleFileChange = (e) => {
    if(e.target.files.length > 0) {
      stateFormData.file.name = e.target.files[0].name
      stateFormData.file.value = e.target.files[0];
    }
  }

  return (
    <div className="row m-3">
      <div className="col-12">
        <form onSubmit={onSubmit} method="POST">
          <div className="form-group">
            <h2>Create Post</h2>
            <hr />
            {stateFormMessage.status === 'error' && (
              <h4 className="warning text-center">{stateFormMessage.error}</h4>
            )}
          </div>
           <input
              className="form-control"
              type="text"
              id="description"
              name="description"
              placeholder="Description"
              onChange={onChange}
              readOnly={loading && true}
              value={stateFormData.description.value}
          />
          <div className="mt-3">
            <input 
                className="form-control" 
                type="file" 
                id="file"
                name="file"
                accept="image/*"
                onChange={handleFileChange}
                readOnly={loading && true}
            />
          </div>
          <div className="mt-3">
            <Link href={{ pathname: '/' }}>
                <a type="button" className="btn btn-danger">Cancel</a>
            </Link>
            <button
              type="submit"
              id="submit"
              className="btn btn-block btn-primary"
              disabled={loading}
            >
              {!loading ? 'Submit' : 'Submitting...'}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
export default FormPost;

