function FormLogin({ props }) {
  const {
    onSubmitHandler,
    onChangeHandler,
    loading,
    stateFormData,
    stateFormError,
    stateFormMessage,
  } = props;

  return (
    <div className="row m-3">
      <div className="col-12">
        <form method="POST" onSubmit={onSubmitHandler}>
          <div className="form-group">
            <h2>Login</h2>
            <hr />
            {stateFormMessage.status === 'error' && (
              <h4 className="warning text-center">{stateFormMessage.error}</h4>
            )}
          </div>
          <div className="form-group">
            <input
              className="form-control"
              type="text"
              id="email"
              name="email"
              placeholder="Email"
              onChange={onChangeHandler}
              readOnly={loading && true}
              value={stateFormData.email.value}
            />
            {stateFormError.email && (
              <span className="warning">{stateFormError.email.hint}</span>
            )}
          </div>
          <div className="mt-3 form-group">
            <input
              className="form-control"
              type="password"
              id="password"
              name="password"
              placeholder="Password"
              onChange={onChangeHandler}
              readOnly={loading && true}
              value={stateFormData.email.password}
            />
            {stateFormError.password && (
              <span className="warning">{stateFormError.password.hint}</span>
            )}
          </div>
          <div className="mt-3">
            <button
              type="submit"
              id="submit"
              className="btn btn-block btn-primary"
              disabled={loading}
            >
              {!loading ? 'Login' : 'Loading...'}
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
export default FormLogin;
