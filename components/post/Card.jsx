
import Moment from 'react-moment';

const Card = ({ data }) => {
  return (
    <>
        <div className="col-lg-4">
        <figure className="figure">
           <img className="figure-img img-fluid rounded" src={data.url || ""}/>
           <figcaption className="figure-caption"><b>Description</b> {data.description || ""}</figcaption>
           <figcaption className="figure-caption"><b>User Name</b>{data.username || ""}</figcaption>
           <figcaption className="figure-caption"><b>Date:</b>
              <Moment format="YYYY/MM/DD">
                {data.createdAt || ""}
              </Moment>
          </figcaption>
        </figure>
        </div>
    </>
  );
};

export default Card;
