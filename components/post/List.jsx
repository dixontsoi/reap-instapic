import Card from "./Card";

const List = ({ post, loading, loadMorePage }) => {
  return (
    <>
        <div className="row mt-3 mb-3">
            {post.map((val, idx) => (
                <Card key={idx} data={val} />
            ))}
        </div>
        <div className="row">
            <div className="col-12">
            <button onClick={loadMorePage} type="button" disabled={loading} className="btn btn-secondary w-100">{loading ? "Loading" : "Load More"}</button>
            </div>
        </div>
    </>
  );
};

export default List;
