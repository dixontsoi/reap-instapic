import Link from 'next/link';
import UserNav from '../navigation/User';
const Header = ({ props, user }) => {
  return (
    <>
    <nav className="navbar navbar-light bg-light">
      <div className="container-fluid">
        <Link href="/">
          <a className="nav-item nav-link">Home</a>
        </Link>
        <div className="d-flex">
          <UserNav props={{ user: user }} />
        </div>
      </div>
    </nav>
    </>
  );
};


export default Header;
