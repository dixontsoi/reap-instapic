# InstanPhoto <br />

# To Getting Start <br />
This project is using Nextjs Framework (React), it combines both frontend and backend in one single project. Also by using cypress to running a integration test. <br />

# Server <br />
Also this project is using AWS Amplify with S3 for file storage <br />

Before running this project, please make sure you have run <br />
`yarn install` or `npm install` <br />

Also please create .env for local enviornment <br />

DB_HOST={{host}} <br />
DB_USER={{username}} <br />
DB_PASS={{password}} <br />
DB_NAME={{dbname}} <br />
PORT={{port}} <br />

ACCESS_KEY_ID={{AWS_ACCESS_KEY}} - Make sure the IAM with S3 Permission <br />
SECRET_ACCESS_KEY={{AWS_SECERT_KEY}} <br />
REGION={{REGION}} <br />
HTTP_OPTIONS={ timeout: 30000} <br />
SIGNATURE_VERSION=v4 <br />
BUCKET={{BUCKET_NAME}} <br />

JWT_KEY="{{RANDOM_STRING}}" <br />

# Commands <br />
yarn dev - start the development server <br />
yarn build - build with webpack <br />
yarn start - start with build file <br />
yarn cypress - start integration test, make sure running yarn dev first<br />


Production Link: <br />
https://main.d192js6wki382k.amplifyapp.com/ <br />

Video For Integeration Test <br />
https://drive.google.com/file/d/11aBGP43S3He1TyK_2SWPIqlvfV1DevZ5/view?usp=sharing <br />


##Database <br />
Create a database <br />
1. Run the sql (db/seeders/table.sql)<br />



API DOC <br />

[POST] /api/auth <br />
Require Parameter: <br />

- email:string <br />
- password:string <br />

Sample: 
<br />
{
    email: dixon.tsoi@abc.com,
    password: randomString
}
<br />
Response: 
<br />
{
    success: true
    token: ""
}
<br />

[POST] /api/user/[slug]
<br />
Require Parameter:
- email:string
- password:string
- username:string
<br />
Sample:
{
    email: "dixon.tsoi@abc.com",
    password: "randomString",
    username: "DixonTsoi"
}
Response:
{
    status: 'success',
    message: 'done',
    data: {
        id: 1,
        username: DixonTsoi,
        email: "dixon.tsoi@abc.com",
        createdAt: "2021-08-24 13:56:57"
        updatedAt: "2021-08-24 13:56:57",
    },
}
<br />

[GET] /api/post/[slug]?page=1
<br />
Require Parameter
-page:number
<br />
Sample:
{
    page: 1
}
<br />

Response: 
<br />

"status":"success",
   "data":[
      {
         "id":15,
         "description":"Tasty Metal Chicken",
         "url":"{s3_url}",
         "username":"Oswaldo.Kuhic",
         "createdAt":"2021-08-24T13:56:57.000Z"
      },
      {
         "id":14,
         "description":"Fantastic Concrete Bike",
         "url":"{s3_url}",
         "username":"Lennie.Champlin76",
         "createdAt":"2021-08-24T13:55:43.000Z"
      },
      {
         "id":13,
         "description":"Licensed Wooden Shoes",
         "url":"{s3_url}",
         "username":"Gladys_Lueilwitz39",
         "createdAt":"2021-08-24T13:54:37.000Z"
      }
   ]
}
[POST] /api/document/[slug]
<br />

Require Parameter:
- name:string
- user:object
-image:binary
<br />
Sample: 
{
    name: image (2).png,
    user: {"id":3,"email":"dixon.tsoi@icw.io","iat":1629815101,"exp":1661372027},
    image: (binary)
}
<br />
Response:
{
   "status":"success",
   "data":{
      "id":42,
      "filePath":"3/",
      "fileName":"image (2).png",
      "updatedAt":"2021-08-24T14:30:28.439Z",
      "createdAt":"2021-08-24T14:30:28.439Z"
   }
}

