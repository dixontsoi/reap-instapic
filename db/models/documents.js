'use strict';
module.exports = (sequelize, DataTypes) => {
  const documents = sequelize.define(
    'documents',
    {
      filePath: DataTypes.STRING,
      fileName: DataTypes.STRING,
    },
    {
      hooks: {
        beforeCreate: function (post, options) { },
      },
    },
  );
  documents.associate = function (models) {
    // associations can be defined here
    documents.hasMany(models.posts);
  };
  return documents;
};
