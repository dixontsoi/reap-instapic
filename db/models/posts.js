'use strict';
module.exports = (sequelize, DataTypes) => {
  const posts = sequelize.define(
    'posts',
    {
      userId: DataTypes.INTEGER,
      documentId: DataTypes.INTEGER,
      description: DataTypes.STRING,
    },
    {
      hooks: {
        beforeCreate: function(post, options) {
          post.description
            .toLowerCase()
            .replace(/[^A-Za-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-');
        },
      },
    },
  );
  posts.associate = function(models) {
    // associations can be defined here
    posts.belongsTo(models.users);
    posts.belongsTo(models.documents);
  };
  return posts;
};
