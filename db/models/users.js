'use strict';
var bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define(
    'users',
    {
      username: DataTypes.STRING,
      password: DataTypes.STRING,
      email: DataTypes.STRING,
    },
    {
      hooks: {
        beforeCreate: async function(user, options) {
          user.password = await bcrypt.hashSync(user.password, 10);
        },
      },
    },
  );
  users.associate = function(models) {
    // associations can be defined here
    users.hasMany(models.posts);
  };
  return users;
};
