import aws from 'aws-sdk';

const BUCKET_NAME = 'instanphoto';
// const s3Config = {
//   accessKeyId: process.env.ACCESS_KEY_ID,
//   secretAccessKey: process.env.SECRET_ACCESS_KEY,
//   region: process.env.REGION,
//   httpOptions: process.env.HTTP_OPTIONS,
//   signatureVersion: process.env.SIGNATURE_VERSION,
// };
const s3Config = {
  accessKeyId: 'AKIA2AXJWY7UKPT5ST4J',
  secretAccessKey: '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',
  region: 'us-east-1',
  httpOptions: { timeout: 30000 },
  signatureVersion: 'v4',
};

const uploadAWSFile = async file => {
  const { filename, value, userId } = file;
  if (!filename && !value && !userId) return;
  let s3 = new aws.S3(s3Config);
  let parmas = {
    Bucket: BUCKET_NAME,
    Key: userId + '/' + filename,
    Body: value,
  };
  let result = await s3.upload(parmas, async (err, data) => {
    if (err) {
      return { success: false, err };
    }
    return data;
  });
  return { success: true, result };
};
const retreiveFile = async key => {
  let s3 = new aws.S3(s3Config);
  const signedUrlExpireSeconds = 60 * 5;
  let signedURL = await s3.getSignedUrl('getObject', {
    Bucket: BUCKET_NAME,
    Key: key,
    Expires: signedUrlExpireSeconds,
  });
  return { url: signedURL };
};

module.exports = {
  uploadAWSFile,
  retreiveFile,
};
