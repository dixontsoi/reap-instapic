import faker from 'faker';

describe('Integration Test', () => {
  it('Phase', () => {
    let email = faker.internet.email();
    let username = faker.internet.userName();
    let password = faker.internet.password();
    let description = faker.commerce.productName();
    const fixtureFile = 'test.jpeg';

    cy.visit('http://localhost:3000/');

    //Register
    cy.visit('http://localhost:3000/user/register');
    cy.get('#username').type(username);
    cy.get('#email').type(email);
    cy.get('#password').type(password);
    cy.get('#submit').click();

    cy.wait(5000);
    //Login
    cy.url().should('include', '/user/login');
    cy.get('#email').type(email);
    cy.get('#password').type(password);
    cy.get('#submit').click();

    cy.wait(5000);
    //Home Page
    cy.url().should('include', '/');
    cy.visit('http://localhost:3000/post/create');
    cy.get('#description').type(description);
    cy.get('input[type="file"]').attachFile(fixtureFile);
    cy.get('#submit').click();

    cy.wait(5000);
    cy.url()
      .should('include', '/')
      .end();
  });
});
